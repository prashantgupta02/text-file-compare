const XLSX = require('xlsx');
const fs = require('fs');


readInputFiles();

async function readInputFiles() {
    let inputData = await readInputFile();
    let mappingData = await readMappingInput();
    let tempArr = [];
    let inputFile = [];
    let mappingFile = [];
    tempArr = inputData.toString().split('\r\n');
    tempArr.forEach(element => {
        inputFile.push(element);
    });
    tempArr = [];
    tempArr = mappingData.toString().split('\r\n');
    tempArr.forEach(element => {
        mappingFile.push(element);
    });

console.log(inputFile);
console.log(mappingFile);
}

async function readInputFile() {
    return new Promise(function (resolve, reject) {
        fs.readFile(process.argv[2], 'utf8', function (err, data) {
            if (err) {
                resolve(err);
            } else {
                resolve(data);
            }
        });
    })
}

async function readMappingInput() {
    return new Promise(function (resolve, reject) {
        fs.readFile(process.argv[2], 'utf8', function (err, data) {
            if (err) {
                resolve(err);
            } else {
                resolve(data);
            }
        });
    })
}


